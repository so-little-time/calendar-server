import { TypeOrmEntity } from "@s2hand/orm-typeorm";
import { Column, Entity } from "typeorm";

@Entity({ name: "chore_date_status" })
export class ChoreDateStatus extends TypeOrmEntity {
  @Column("varchar", { name: "chore_id" })
  public choreId!: string;

  @Column("varchar", { name: "chore_date_id" })
  public choreDateId!: string;

  public get idPrefix(): string {
    return "chore_date_status";
  }
}
