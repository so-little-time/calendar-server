import { TypeOrmEntity } from "@s2hand/orm-typeorm";
import { Column, Entity } from "typeorm";

@Entity({ name: "chore_date" })
export class ChoreDate extends TypeOrmEntity {
  @Column("date", { name: "value" })
  public value!: Date;

  public get idPrefix(): string {
    return "chore_date";
  }
}
