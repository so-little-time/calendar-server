import { TypeOrmEntity } from "@s2hand/orm-typeorm";
import { Column, Entity, JoinTable, ManyToMany } from "typeorm";

import { ChoreDate } from "./chore-date.entity";

@Entity({ name: "chore" })
export class Chore extends TypeOrmEntity {
  @Column("varchar", { name: "name" })
  public name!: string;

  @Column("date", { name: "start_date", nullable: true })
  public startDate: Date | null = null;

  @Column("date", { name: "end_date", nullable: true })
  public endDate: Date | null = null;

  @ManyToMany(() => ChoreDate, { eager: true })
  @JoinTable({
    name: "chore_date_status",
    joinColumn: { name: "chore_id", referencedColumnName: "id" },
    inverseJoinColumn: { name: "chore_date_id", referencedColumnName: "id" },
  })
  public choreDates!: ChoreDate[];

  public get idPrefix(): string {
    return "chore";
  }
}
