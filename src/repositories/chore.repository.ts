import { Chore } from "@/entities/chore.entity";
import { TypeOrmRepository } from "@s2hand/orm-typeorm";
import { EntityRepository } from "typeorm";

@EntityRepository(Chore)
export class ChoreRepository extends TypeOrmRepository<Chore> {}
