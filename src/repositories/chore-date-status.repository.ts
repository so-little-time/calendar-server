import { ChoreDateStatus } from "@/entities/chore-date-status.entity";
import { ChoreStatus } from "@/types/chore.type";
import { TypeOrmRepository } from "@s2hand/orm-typeorm";
import { EntityRepository } from "typeorm";

@EntityRepository(ChoreDateStatus)
export class ChoreDateStatusRepository extends TypeOrmRepository<ChoreDateStatus> {
  public async updateChoreStatus(
    choreId: string,
    choreDateId: string,
    status: ChoreStatus,
  ): Promise<void> {
    const options = {
      choreId,
      choreDateId,
    };
    if (status === ChoreStatus.Pending) {
      await this.delete(options);
      return;
    }
    let choreDateStatus = await this.findOne(options);
    if (!choreDateStatus) {
      choreDateStatus = this.create(options);
      await this.save(choreDateStatus);
    }
  }
}
