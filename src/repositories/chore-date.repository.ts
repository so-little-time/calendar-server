import { ChoreDate } from "@/entities/chore-date.entity";
import { TypeOrmRepository } from "@s2hand/orm-typeorm";
import { EntityRepository } from "typeorm";

@EntityRepository(ChoreDate)
export class ChoreDateRepository extends TypeOrmRepository<ChoreDate> {}
