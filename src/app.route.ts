import express from "express";

import dailyRouter from "./modules/daily/daily.route";

const router = express.Router();

router.get("/", (req, res) =>
  res.send("Express with Typescript, God of Setup"),
);

router.use("/daily", dailyRouter);

export default router;
