import { ChoreDateStatusRepository } from "@/repositories/chore-date-status.repository";
import { ChoreDateRepository } from "@/repositories/chore-date.repository";
import { ChoreStatus } from "@/types/chore.type";
import autoBind from "auto-bind";
import { getManager } from "typeorm";

class _DailyAction {
  // TODO: Update "exmic" package, base "auto-bind"
  constructor() {
    autoBind(this);
  }

  public async updateChoreStatus(
    choreId: string,
    date: string,
    status: ChoreStatus = ChoreStatus.Pending,
  ): Promise<void> {
    return getManager().transaction(async (entityManager) => {
      const choreDateRepository =
        entityManager.getCustomRepository(ChoreDateRepository);
      const choreDateStatusRepository = entityManager.getCustomRepository(
        ChoreDateStatusRepository,
      );

      let choreDate = await choreDateRepository.findOne({
        value: date,
      });
      if (!choreDate) {
        choreDate = choreDateRepository.create({
          value: new Date(date),
        });
        choreDate = await choreDateRepository.save(choreDate);
      }

      await choreDateStatusRepository.updateChoreStatus(
        choreId,
        choreDate.id,
        status,
      );
    });
  }
}

const DailyAction = new _DailyAction();
export default DailyAction;
