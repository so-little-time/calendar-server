import { ChoreRepository } from "@/repositories/chore.repository";
import { BaseController } from "exmic/base";
import express from "express";
import {
  LessThan,
  LessThanOrEqual,
  MoreThan,
  MoreThanOrEqual,
  Not,
  getCustomRepository,
} from "typeorm";

import DailyAction from "./daily.action";

class _DailyController extends BaseController {
  public async updateChoreStatus(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) {
    try {
      const { choreId } = req.params;
      const { date, status } = req.body;
      const result = await DailyAction.updateChoreStatus(choreId, date, status);
      return this.success(req, res)(result);
    } catch (e) {
      next(this.getManagedError(e));
    }
  }

  public async getChores(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) {
    try {
      const { rangeDateStart, rangeDateEnd } = req.query;
      // TODO: Move logic out of controller
      let options = {};
      if (rangeDateStart && !rangeDateEnd) {
        options = [
          { endDate: null },
          { endDate: MoreThanOrEqual(rangeDateStart) },
        ];
      } else if (rangeDateEnd && !rangeDateStart) {
        options = [
          { startDate: null },
          { startDate: LessThanOrEqual(rangeDateEnd) },
        ];
      } else if (rangeDateStart && rangeDateEnd) {
        options = [
          { startDate: null, endDate: null },
          { startDate: null, endDate: MoreThanOrEqual(rangeDateStart) },
          { startDate: LessThanOrEqual(rangeDateEnd), endDate: null },
          {
            startDate: Not(MoreThan(rangeDateEnd)),
            endDate: Not(LessThan(rangeDateStart)),
          },
        ];
      }
      const result = await this._choreRepository
        .createQueryBuilder("chore")
        .leftJoinAndSelect("chore.choreDates", "choreDate")
        .where(options)
        .getMany();
      return this.success(req, res)(result);
    } catch (e) {
      next(this.getManagedError(e));
    }
  }

  public async createChore(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) {
    try {
      const data = req.body;
      const chore = this._choreRepository.create({ ...data });
      const result = await this._choreRepository.save(chore);
      return this.success(req, res)(result);
    } catch (e) {
      next(this.getManagedError(e));
    }
  }

  public async updateChore(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) {
    try {
      const { choreId } = req.params;
      const data = req.body;
      await this._choreRepository.update({ id: choreId }, data);
      const chore = await this._choreRepository.findOne({ id: choreId });
      return this.success(req, res)(chore);
    } catch (e) {
      next(this.getManagedError(e));
    }
  }

  private get _choreRepository(): ChoreRepository {
    return getCustomRepository(ChoreRepository);
  }
}

const DailyController = new _DailyController("DAILY_CONTROLLER");
export default DailyController;
