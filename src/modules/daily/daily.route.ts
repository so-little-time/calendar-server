import express from "express";

import DailyController from "./daily.controller";

const dailyRouter = express.Router({ mergeParams: true });

const { updateChoreStatus, getChores, createChore, updateChore } =
  DailyController;
// TODO: Add express-validator
dailyRouter.put("/chores/:choreId", updateChore);
dailyRouter.put("/chores/:choreId/status", updateChoreStatus);
dailyRouter.get("/chores", getChores);
dailyRouter.post("/chores", createChore);

export default dailyRouter;
