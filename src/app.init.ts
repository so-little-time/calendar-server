import path from "path";

import dotenv from "dotenv";
import { Connection, createConnection } from "typeorm";

import { ChoreDateStatus } from "./entities/chore-date-status.entity";
import { ChoreDate } from "./entities/chore-date.entity";
import { Chore } from "./entities/chore.entity";

const initEnvironments = (): void => {
  dotenv.config({
    path: path.join(process.cwd(), "environments", ".env.common"),
  });
  dotenv.config({
    path: path.join(
      process.cwd(),
      "environments",
      `.env.${process.env.NODE_ENV}`,
    ),
  });
};

const initMysqlConnection = async (): Promise<Connection> => {
  const connection = await createConnection({
    type: "mysql",
    host: process.env.MYSQL_HOST || "localhost",
    port: Number(process.env.MYSQL_TCP_PORT) || 3306,
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    entities: [Chore, ChoreDate, ChoreDateStatus],
  });
  return connection;
};

export { initEnvironments, initMysqlConnection };
