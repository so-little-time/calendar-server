import "reflect-metadata";

import path from "path";

import cors from "cors";
import { LogHelper } from "exmic/helpers";
import { ErrorMiddleware } from "exmic/middlewares";
import express from "express";

import { initEnvironments, initMysqlConnection } from "./app.init";
import router from "./app.route";

// express
const app = express();
initEnvironments();

app.use(cors());
app.use(express.static(path.join(process.cwd(), "public")));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
// TODO: Add global string trimming
app.use("/", router);
app.use(ErrorMiddleware.handleAppError);

setImmediate(async () => {
  try {
    await initMysqlConnection();
    LogHelper.info("APP", "Database connected");
  } catch (e) {
    LogHelper.error("APP", "First start failed to connect to DB", e);
  }

  app.listen(process.env.APP_INTERNAL_PORT, () => {
    LogHelper.info(
      "APP",
      "Local/Clustered Server started at",
      `http://localhost:${process.env.APP_INTERNAL_PORT}`,
    );
    LogHelper.info(
      "APP",
      "Docker exposed Container Server started at",
      `http://localhost:${process.env.APP_DOCKER_EXPOSED_PORT}`,
    );
  });
});
